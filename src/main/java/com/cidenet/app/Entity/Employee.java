package com.cidenet.app.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employees")
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long id;
	@Column(length = 20, nullable = false)
	private String name;
	@Column(length = 50)
	private String second_name;
	@Column(length = 20)
	private String surmane;
	@Column(length = 20)
	private String second_surname;
	@Column(length = 300, nullable = false,unique = true)
	private String email;
	@Column(length = 10, nullable = false)
	private String country_work;
	@Column(length = 25, nullable = false)
	private String area;
	@Column(length = 25, nullable = false)
	private String document;
	@Column(length = 25, nullable = false)
	private String document_type;
	private String record_add;
	private String record_update;
	private String hire_date;
	private Boolean state ;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecond_name() {
		return second_name;
	}
	public void setSecond_name(String second_name) {
		this.second_name = second_name;
	}
	public String getSurmane() {
		return surmane;
	}
	public void setSurmane(String surmane) {
		this.surmane = surmane;
	}
	public String getSecond_surname() {
		return second_surname;
	}
	public void setSecond_surname(String second_surname) {
		this.second_surname = second_surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCountry_work() {
		return country_work;
	}
	public void setCountry_work(String country_work) {
		this.country_work = country_work;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getDocument_type() {
		return document_type;
	}
	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}
	public String getRecord_add() {
		return record_add;
	}
	public void setRecord_add(String record_add) {
		this.record_add = record_add;
	}
	public String getRecord_update() {
		return record_update;
	}
	public void setRecord_update(String record_update) {
		this.record_update = record_update;
	}
	public String getHire_date() {
		return hire_date;
	}
	public void setHire_date(String hire_date) {
		this.hire_date = hire_date;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
