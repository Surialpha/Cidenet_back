package com.cidenet.app.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.app.Entity.Employee;
import com.cidenet.app.Service.EmployeeService;

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE  })
@RestController
@RequestMapping("api/employees")
public class EmployeeController {
	
	@Autowired
	private EmployeeService emploService;
	
	
	
	//Create a new Employee
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Employee employee){
		
		try {
			
			//Validate that documents didn't exist in the database
			if(findDocumentAndDocument_Type(employee.getDocument(),employee.getDocument_type())) {
				String msg = "There are a document and document type with the same data, please verify";
				return new ResponseEntity<>(msg, HttpStatus.CONFLICT);
			}
			//Create email variable 
			String emailVar = employee.getName()
					.replaceAll(" ", "")
					.toLowerCase()+"."+
					employee.getSurmane().
					replaceAll(" ", "").
					toLowerCase()+
					ReturnCountryDomain(employee.getCountry_work());
			
			//If email already exist, put the last index of the table in front of the name and surname
			if(findEmail(emailVar)) {
				emailVar = employee.getName()
						.replaceAll(" ", "")
						.toLowerCase()+"."+
						employee.getSurmane().
						replaceAll(" ", "").
						toLowerCase()+"."+
						ReturnmaxId()+
						ReturnCountryDomain(employee.getCountry_work());
			}
			
			//Create the date to be stored like a date of creation
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("DD/MM/yyyy HH:mm:ss");
			employee.setRecord_add(formatter.format(date));
			employee.setEmail(emailVar);
			employee.setState(true);
			employee.setName(employee.getName().toUpperCase());
			employee.setSurmane(employee.getSurmane().toUpperCase());
			employee.setSecond_name(employee.getSecond_name().toUpperCase());
			employee.setSecond_surname(employee.getSecond_surname().toUpperCase());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(emploService.save(employee));
		} catch (Exception e) {
			String msg = "Something went wrong, try again";
			return new ResponseEntity<>(msg, HttpStatus.CONFLICT);
		}
		
	}
	
	//If email already exist, put the last index of the table in front of the name and surname return:? true : false
	private Boolean findEmail(String email) {
			Optional<Employee> result = emploService.findByEmail(email);
			return result.isPresent();
	}
	
	//Validate that documents didn't exist in the database return:? true : false
	private Boolean findDocumentAndDocument_Type(String document, String document_Type) {
			Optional<Employee> result = emploService.findByDocumentAndDocumentType(document, document_Type);
			return result.isPresent();
	}
	
	
	//Put the email domain country depending of the country work
	private String ReturnCountryDomain(String country_work) {
		String domainCountry ="@cidenet.com.co";
		if(country_work.toLowerCase().equals("us")) {
			domainCountry ="@cidenet.com.us";
		}
		return domainCountry;
	}
	
	//Return the max id in the table to put it as a unique id in front of the email that require it 
	private String ReturnmaxId() {
		return emploService.getMaxId();
	}
	
	
	//Select by {id}, return just one Object
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable Long id){
		Optional<Employee> result = emploService.findById(id);
		
		if(!result.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).body(result);
	}
	
	//Select all, return list of Objects
	@GetMapping
	public ResponseEntity<?> getAll(){
		return ResponseEntity.status(HttpStatus.CREATED).body(emploService.findAll());
	}
	

	//Update function
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Employee employee, @PathVariable Long id){
		Optional<Employee> emploResult = emploService.findById(id);
		
		if(!emploResult.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		//if Document or Document type are different, it indicate that we need to search if there are 
		//the same values in the database, if don't, code keep running normally  
		if(!employee.getDocument().equals(emploResult.get().getDocument()) 
				|| !employee.getDocument_type().equals(emploResult.get().getDocument_type()) ) {
			
			//Same function as create  line:36
			if(findDocumentAndDocument_Type(employee.getDocument(),employee.getDocument_type())) {
				String msg = "There are a document and document type with the same data, please verify";
				return new ResponseEntity<>(msg, HttpStatus.CONFLICT);
			}
		}
		
		//Set the current values of the fields that change the email address
		String Name = emploResult.get().getName();
		String Surname = emploResult.get().getSurmane();
		String Country = emploResult.get().getCountry_work();
		String emailVar = "";
		
		Boolean changeEmail = false;
		
		//If any of the fields are no the same that old record, turn ChangeEmail true
		if(!employee.getName().toUpperCase().equals(emploResult.get().getName())) {
			Name = employee.getName().toUpperCase();
			changeEmail=true;
		}
		if(!employee.getSurmane().toUpperCase().equals(emploResult.get().getSurmane())) {
			Surname = employee.getSurmane().toUpperCase();
			changeEmail=true;
		}
		if(!employee.getCountry_work().toUpperCase().equals(emploResult.get().getCountry_work())) {
			Country = employee.getCountry_work().toLowerCase();
			changeEmail=true;
		}
		//In case that ChangeEmail is set true, change the current email for a new correct email
		if(changeEmail) {
			//same process of create method line:41
			emailVar = Name
					.replaceAll(" ", "")
					.toLowerCase()+"."+
					Surname
					.replaceAll(" ", "")
					.toLowerCase()+
					ReturnCountryDomain(Country);
			
			if(findEmail(emailVar)) {
				emailVar = Name
						.replaceAll(" ", "")
						.toLowerCase()+"."+
						Surname
						.replaceAll(" ", "")
						.toLowerCase()+"."+
						ReturnmaxId()+
						ReturnCountryDomain(Country);
			}
			emploResult.get().setEmail(emailVar);
			
		}else {
			emploResult.get().setEmail(employee.getEmail());
		}
		
		//Set old record with new values
		emploResult.get().setName(employee.getName().toUpperCase());
		emploResult.get().setSecond_name(employee.getSecond_name().toUpperCase());
		emploResult.get().setSurmane(employee.getSurmane().toUpperCase());
		emploResult.get().setSecond_surname(employee.getSecond_surname().toUpperCase());
		//Get current time to setting up the updating date 
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("DD/MM/yyyy HH:mm:ss");
		emploResult.get().setRecord_update(formatter.format(date));
		emploResult.get().setArea(employee.getArea());
		emploResult.get().setCountry_work(employee.getCountry_work());
		emploResult.get().setDocument(employee.getDocument());
		emploResult.get().setDocument_type(employee.getDocument_type());
		emploResult.get().setState(employee.getState());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(emploService.save(emploResult.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id){
		Optional<Employee> emploResult = emploService.findById(id);
		
		if(!emploResult.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		emploService.deletebyId(id);
		return ResponseEntity.ok().build();
	}
	
	
	
}
