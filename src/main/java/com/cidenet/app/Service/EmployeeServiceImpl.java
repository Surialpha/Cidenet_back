package com.cidenet.app.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cidenet.app.Entity.Employee;
import com.cidenet.app.Repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Employee> findAll() {
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Employee> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Employee> findById(Long id) {
		return repository.findById(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Employee> findByEmail(String email) {
		return repository.findByEmail(email);
	}
	
	@Override
	@Transactional
	public Employee save(Employee employee) {
		return repository.save(employee);
	}

	@Override
	@Transactional
	public void deletebyId(Long id) {
		repository.deleteById(id);
	}

	@Override
	public String getMaxId() {
		return repository.getMaxId();
	}

	@Override
	public Optional<Employee> findByDocumentAndDocumentType(String document, String document_type) {
		return repository.findByDocumentAndDocumentType(document, document_type);
	}


}
