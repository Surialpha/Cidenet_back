package com.cidenet.app.Service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cidenet.app.Entity.Employee;

public interface EmployeeService {

	public Iterable<Employee> findAll();
	
	public Page<Employee> findAll(Pageable pageable);
	
	public Optional<Employee> findById(Long id);
	
	public Optional<Employee> findByEmail(String email);
	
	public Optional<Employee> findByDocumentAndDocumentType(String document, String document_type);
	
	public Employee save(Employee employee);
	
	public void deletebyId(Long id);
	
	public String getMaxId();
}


