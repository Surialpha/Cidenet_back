package com.cidenet.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CidenetAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CidenetAppApplication.class, args);
	}

}
