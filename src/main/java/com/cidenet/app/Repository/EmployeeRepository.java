package com.cidenet.app.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cidenet.app.Entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	@Query(value = "SELECT e.* FROM cidenet.employees e WHERE e.email = ?1 ",nativeQuery = true)
	public Optional<Employee> findByEmail(String email);
	
	@Query(value = "SELECT MAX(e.id) FROM cidenet.employees e",nativeQuery = true)
	public String getMaxId();
	
	@Query(value = "SELECT e.* FROM cidenet.employees e WHERE e.document = ?1 AND e.document_type = ?2  ",nativeQuery = true)
	public Optional<Employee> findByDocumentAndDocumentType(String document, String document_Type);
	
}
